
  export const darkMode = {
    backgroundColor: '#f5f5f5',
    color: 'white',
    padding:'5rem'
  };
  export const contentlightmode= {
    backgroundColor:'#131111',
    color:'#000',
    padding:'5rem'
  }

  export const toggleColumn={
    backgroundColor:'#f5f5f5'
  }
  export const togglecolumnLight ={
    backgroundColor: '#131111'
  }

  export const navdarkmode={
    backgroundColor:'#000',
    display:'flex',
    justifyContent:'center',
    alignItems:'center'
  }
  
  export const navlightmode ={
    backgroundColor:'#fff',
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'column'
  }
  export const toggleButton={
    backgroundColor:'hsla(0,0%,100%,.2)',
    borderRadius:'30px 0 0 30px',
    width:'50px',
    padding:'8px',
    flexDirection:'column'
  }

