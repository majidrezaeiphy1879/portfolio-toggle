/**
 * This service module allows for easy validation of strings
 */

export default {
    rules: {
        USER_REGEX:  /^[A-z][A-z0-9-_]{3,23}$/,
        PWD_REGEX: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/
    },
    isPasswordValid(password) {
        return this.rules.PWD_REGEX.test(password);
    },
    isUserNameValid(userName) {
        return this.rules.USER_REGEX.test(userName);
    }
}
