import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import About from "../pages/About";
import NavbarMenue from "../components/Navbar";
import { BrowserRouter, Route, Routes } from "react-router-dom";
function Root() {
  return (
    <BrowserRouter>
      <Container fluid>
        <Row style={{ vh: "100" }}>
          <Col md={3} style={{ backgroundColor: "green", vh: "100%" }}>
            <NavbarMenue />
          </Col>
          <Col>
            <Routes>
                <Route path='/about' element={<About/>}/>
                <Route path='/about' element={<About/>}/>
                <Route path='/about' element={<About/>}/>
                <Route path='/about' element={<About/>}/>
                <Route path='/about' element={<About/>}/>
                <Route path='/about' element={<About/>}/>
            </Routes>
          </Col>
        </Row>
      </Container>
    </BrowserRouter>
  );
}

export default Root;
