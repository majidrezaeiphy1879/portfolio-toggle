import React, { useState, useReducer, useRef, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import axios from "axios";
import Login from "./Login";
import Form from "react-bootstrap/Form";
import Image from "react-bootstrap/Image";
import greenMarker from "../images/authIcons/tick-svgrepo-com.svg";
import redMarker from "../images/authIcons/cross-svgrepo-com.svg";
import Validation from "../services/Validation";
const USER_REGEX = Validation.rules.USER_REGEX;
const PWD_REGEX = Validation.rules.PWD_REGEX;

//const REGISTER_URL = "/";
const initialState = {
  user: "",
  validName: false,
  userFocus: false,
  pwd: "",
  validPwds: false,
  pwdFocus: false,
  matchPwd: "",
  validMatch: false,
  matchFocus: false,
  errMsg: "",
  success: false,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "SET_USER":
      return { ...state, user: action.payload };
    case "SET_VALID_NAME":
      return { ...state, validName: action.payload };
    case "SET_PWD":
      return { ...state, pwd: action.payload };
    case "SET_VALID_PWD":
      return { ...state, validPwd: action.payload };
    case "SET_PWD_MATCH":
      return { ...state, validMatch: action.payload };
    case "SET_SUCCESS":
      return { ...state, success: action.payload };
    case "SET_ERR_MSG":
      return { ...state, errMsg: action.payload };
    default:
      return state;
  }
};

function Register({ toggleRegister }) {
  const userFocus = useRef();
  const errRef = useRef();
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    userFocus.current.focus();
  }, []);

  const handleUserChange = (e) => {
    const username = e.target.value;
    const isValid = USER_REGEX.test(username);
    dispatch({ type: "SET_USER", payload: username });
    dispatch({ type: "SET_VALID_NAME", payload: isValid });
  };
  const handlePwdChange = (e) => {
    const pwd = e.target.value;
    const isValid = PWD_REGEX.test(pwd);
    dispatch({ type: "SET_PWD", payload: pwd });
    dispatch({ type: "SET_VALID_PWD", payload: isValid });
    console.log(pwd);
  };
  const handlePwdMatch = (e) => {
    const matchingPwd = e.target.value;
    const isValid = matchingPwd === state.pwd;
    dispatch({ type: "SET_PWD_MATCH", payload: isValid });
    console.log(matchingPwd);
  };
  // const handleSubmit = async (e) => {
  //   e.preventDefault();
  //   const v1 = USER_REGEX.test(state.user);
  //   const v2 = PWD_REGEX.test(state.pwd);
  //   if (!v1 || !v2) {
  //     dispatch({ type: "SET_ERR_MSG", payload: "Invalid Entry" });
  //     return;
  //   }
  //   try {
  //     const response = await axios.post(
  //       REGISTER_URL,
  //       JSON.stringify({ user: state.user, pwd: state.pwd }),
  //       {
  //         headers: { "Content-Type": "application/json" },
  //         withCredentials: true,
  //       }
  //     );
  //     console.log(response?.data);
  //     console.log(response?.accessToken);
  //     console.log(JSON.stringify(response));
  //     dispatch({ type: "SET_SUCCESS", payload: true });
  //   } catch (err) {
  //     if (!err?.response) {
  //       dispatch({ type: "SET_ERR_MSG", payload: "No Server Response" });
  //     } else if (err.response?.status === 409) {
  //       dispatch({ type: "SET_ERR_MSG", payload: "Username Taken" });
  //     } else {
  //       dispatch({ type: "SET_ERR_MSG", payload: "Registration Failed" });
  //     }
  //     errRef.current.focus();;
  //   }
  // };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const v1 = USER_REGEX.test(state.user);
    const v2 = PWD_REGEX.test(state.pwd);
    if (!v1 || !v2) {
      dispatch({ type: "SET_ERR_MSG", payload: "Invalid Entry" });
      return;
    }
    console.log(state.user, state.pwd);
    // a successful registration
    dispatch({ type: "SET_SUCCESS", payload: true });
  };
  return (
    <Container>
      <Row>
        <Col
          style={{ border: "2px #8492d0 solid", borderRadius: "15px" }}
          className="p-3"
        >
          {state.success ? (
            <Container>
              <Row>
                <Col className="d-flex justify-content-center pt-3">
                  <span>
                    <p className="fw-bold text-primary">
                      You registred Succesfully, Now go to Login page
                    </p>
                  </span>
                </Col>
              </Row>
            </Container>
          ) : (
            <Form>
              <Row>
                <h3 className="text-primary">Register</h3>
                <Form.Group
                  controlId="formGridEmail"
                  className="mb-3 text-primary"
                >
                  {state.validName ? (
                    <div>
                      <Form.Label>User Name</Form.Label>
                      <Image
                        src={greenMarker}
                        style={{ width: "40px", height: "40px" }}
                        roundedCircle
                      />
                    </div>
                  ) : (
                    <>
                      <Form.Label>User Name</Form.Label>
                      <Image
                        src={redMarker}
                        style={{ width: "40px", height: "40px" }}
                        roundedCircle
                      />
                    </>
                  )}
                  <Form.Control
                    type="text"
                    placeholder="User Name"
                    className="mb-2"
                    autoComplete="off"
                    required
                    isInvalid={!state.validName}
                    ref={userFocus}
                    onChange={handleUserChange}
                    aria-describedby="userInputValidation"
                  />

                  {state.validName ? null : (
                    <Form.Control.Feedback type="invalid">
                      {state.user.length < 4
                        ? "Username must be at least 4 characters."
                        : !/^[A-Za-z]/.test(state.user)
                        ? "Username must start with a letter."
                        : "Username can only contain letters, numbers, underscores, and hyphens."}
                    </Form.Control.Feedback>
                  )}
                </Form.Group>
                <Form.Group
                  controlId="formGridPassword"
                  className="mb-3 text-primary"
                >
                  {state.validPwd ? (
                    <div>
                      <Form.Label>Password</Form.Label>
                      <Image
                        src={greenMarker}
                        style={{ width: "40px", height: "40px" }}
                        roundedCircle
                      />
                    </div>
                  ) : (
                    <>
                      <Form.Label>Password</Form.Label>
                      <Image
                        src={redMarker}
                        style={{ width: "40px", height: "40px" }}
                        roundedCircle
                      />
                    </>
                  )}
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    className="mb-2"
                    isInvalid={!state.validPwd}
                    onChange={handlePwdChange}
                    aria-describedby="userInputValidation"
                    ref={errRef}
                  />
                  {state.validPwd ? null : (
                    <Form.Control.Feedback type="invalid">
                      {state.pwd.length < 8
                        ? "Password must be at least 8 characters."
                        : !/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%])/.test(
                            state.pwd
                          )
                        ? "Must include uppercase and lowercase letters, a number, and one of the following special characters: ! @ # $ %"
                        : "Allowed special characters: ! @ # $ %"}
                    </Form.Control.Feedback>
                  )}
                </Form.Group>
              </Row>

              <Form.Group
                controlId="formGridPassword"
                className="mb-3 text-primary"
              >
                {state.validMatch ? (
                  <div>
                    <Form.Label>Confirm Password</Form.Label>
                    <Image
                      src={greenMarker}
                      style={{ width: "40px", height: "40px" }}
                      roundedCircle
                    />
                  </div>
                ) : (
                  <>
                    <Form.Label>Confirm Password</Form.Label>
                    <Image
                      src={redMarker}
                      style={{ width: "40px", height: "40px" }}
                      roundedCircle
                    />
                  </>
                )}

                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  className="mb-3"
                  onChange={handlePwdMatch}
                  isInvalid={!state.validMatch}
                  ref={errRef}
                />
                {state.validMatch ? null : (
                  <Form.Control.Feedback type="invalid">
                    Passwords do not match.
                  </Form.Control.Feedback>
                )}
              </Form.Group>

              <Form.Group
                className="mb-3 text-primary"
                controlId="formGridAddress2"
              >
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter Your Email"
                  className="mb-3"
                />
              </Form.Group>

              <Form.Group className="mb-3" id="formGridCheckbox">
                <Form.Check
                  type="checkbox"
                  label="Remember"
                  className="text-primary"
                />
              </Form.Group>
              <p className="text-primary">
                Already have an account?
                <a
                  href="#"
                  className="text-warning ps-1"
                  onClick={toggleRegister}
                >
                  Login
                </a>
              </p>
              <Button
                variant="primary"
                onClick={handleSubmit}
                disabled={
                  !state.validPwd || !state.validName || !state.validMatch
                }
              >
                Register
              </Button>
            </Form>
          )}
        </Col>
      </Row>
    </Container>
  );
}

export default Register;
