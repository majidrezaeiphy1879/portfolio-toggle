import React from 'react'
import nightIcon from '../images/nightIcon.png'
import sunIcon from '../images/sunIcon.svg'
import { useContext } from 'react'
import { modeContext } from '../context/ModeContext'
function ModeToggle() {
  const {mode} = useContext(modeContext)
  return (
    <label style={{cursor: 'pointer'}}>
       {mode? <img src={sunIcon} style={{height:'30px', width:'30px', borderRadius:'40px'}} alt="" />:
        <img src={nightIcon} style={{height:'30px', width:'30px', borderRadius:'40px'}} alt="" />}
    </label>
  )
}

export default ModeToggle;