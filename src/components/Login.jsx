import React, { useEffect, useReducer, useRef } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Link } from "react-router-dom";
import Dashboard from "./Dashboard";
const initialState = {
  user: "",
  pwd: "",
  errMsg: "",
  success: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "SET_USER":
      return { ...state, user: action.payload };
    case "SET_PWD":
      return { ...state, pwd: action.payload };
    case "RESET_ERR":
      return { ...state, errMsg: "" };
    case "SET_SUCCESS":
      return { ...state, success: action.payload };
    default:
      return state;
  }
};

function Login({ toggleRegister }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const userRef = useRef();
  const errRef = useRef();
  useEffect(() => {
    if (userRef.current) {
      userRef.current.focus();
    }
  }, []);
  useEffect(() => {
    dispatch({ type: "RESET_ERR" });
  }, [state.user, state.pwd]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    dispatch({ type: "SET_USER", payload: "" });
    dispatch({ type: "SET_PWD", payload: "" });
    dispatch({ type: "SET_SUCCESS", payload: true });
  };
  return (
    <>
    {state.success ? <Dashboard/>:<Container>
      <Row
        className=" p-3 "
        style={{ border: "2px #8492d0 solid", borderRadius: "15px" }}
      >
        <Col>
          <Form >
            <h3 className="text-primary">Login</h3>
            <Form.Group
              className="mb-3 text-primary"
              controlId="formBasicEmail"
            >
              <Form.Label>User Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="User Name"
                ref={userRef}
                onChange={(e) =>
                  dispatch({ type: "SET_USER", payload: e.target.value })
                }
                value={state.user}
                required
              />
            </Form.Group>

            <Form.Group
              className="mb-3 text-primary"
              controlId="formBasicPassword"
            >
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={state.pwd}
                required
                onChange={(e) =>
                  dispatch({ type: "SET_PWD", payload: e.target.value })
                }
              />
            </Form.Group>

            <p className="text-primary">
              Don't have an account?
              <a
                href="#"
                className="text-warning ms-1"
                onClick={toggleRegister}
              >
                Register
              </a>
            </p>
            <Button variant="primary" onClick={handleSubmit}>
              Login
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>}
    </>
  );
}

export default Login;
