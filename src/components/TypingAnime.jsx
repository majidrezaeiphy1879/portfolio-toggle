import React, { useState, useEffect } from 'react';
import { useContext } from 'react';
import { modeContext } from '../context/ModeContext';
function TypingEffect({ words }) {
  const {mode}=useContext(modeContext)
  const [currentWordIndex, setCurrentWordIndex] = useState(0);
  const [currentLetterIndex, setCurrentLetterIndex] = useState(0);
  const [typedText, setTypedText] = useState('');

  useEffect(() => {
    const typingTimer = setInterval(() => {
      if (currentWordIndex < words.length) {
        const currentWord = words[currentWordIndex];
        if (currentLetterIndex < currentWord.length) {
          setTypedText(currentWord.substring(0, currentLetterIndex + 1));
          setCurrentLetterIndex(currentLetterIndex + 1);
        } else {
          setCurrentWordIndex(currentWordIndex + 1);
          setCurrentLetterIndex(0);
        }
      } else {
        setCurrentWordIndex(0);
        setTypedText('');
      }
    }, 100); 

    return () => clearInterval(typingTimer);
  }, [currentLetterIndex, currentWordIndex, words]);

  return <span  style= {{padding:'4px'}} className={mode ? 'typeanimeLight':'typeanimeDark'}>{typedText}</span>;
}

export default TypingEffect;
