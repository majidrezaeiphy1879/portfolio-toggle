import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Figure from "react-bootstrap/Figure";
import axios from "axios";
import { motion } from "framer-motion";
function Dashboard() {
  const [images, setImages] = useState([]);

  useEffect(() => {
    const fetchFantasyImages = async () => {
      try {
        const response = await axios(
          "https://api.scryfall.com/cards/search?order=cmc&q=c:red%20pow=3"
        );

        const selectedImages = [];

        for (let i = 0; i < 8; i++) {
          const randomIndex = Math.floor(
            Math.random() * response.data.data.length
          );
          selectedImages.push(
            response.data.data[randomIndex].image_uris.normal
          );
        }

        setImages(selectedImages);
      } catch (err) {
        console.log(err);
      }
    };

    fetchFantasyImages();
  }, []);

  const colImages = images.map((img, index) => (
    <Col key={index} sm={6} md={3}>
      <motion.div whileHover={{ scale: 2 }}>
        <motion.Figure>
          <Figure.Image width={171} height={180} alt="171x180" src={img} />
        </motion.Figure>
      </motion.div>
    </Col>
  ));

  const rows = [];
  for (let i = 0; i < colImages.length; i += 4) {
    rows.push(colImages.slice(i, i + 4));
  }

  return (
    <Container fluid>
      {rows.map((row, index) => (
        <Row key={index}>{row}</Row>
      ))}
    </Container>
  );
}

export default Dashboard;
