import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import Navbar from "react-bootstrap/Navbar";
import Offcanvas from "react-bootstrap/Offcanvas";
import { Link } from "react-router-dom";
import HomeDarkIcon from "../images/homeNightIcon.svg";
import HomeLightIcon from "../images/homeLightIcon.svg";
import Aboutlight from "../images/aboutLight.svg";
import AboutDark from "../images/aboutDark.svg";
import ServiceDark from "../images/serviceDark.svg";
import ServiceLight from "../images/servicelight.svg";
import PortfolioDark from "../images/portfolioDark.svg";
import PortfolioLight from "../images/portfolioLight.svg";
import ContactDark from "../images/contactDark.svg";
import ContactLight from "../images/contactLight.svg";
import loginDark from "../images/logIndark.svg";
import loginLight from "../images/logInLight.svg";
import ModeToggle from "./ModeToggle";

import { useContext } from "react";
import { modeContext } from "../context/ModeContext";

function NavbarMenue() {
  const { mode, dispatch } = useContext(modeContext);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <Container fluid>
      <Row
        className="align-items-md-end"
        style={{ paddingLeft: "4rem", paddingRight: "5px" }}
      >
        <Col>
          <span>
            <p className={mode ? "navbar-cardiffDark" : "navbar-cardiffLight"}>
              CARDIFF
            </p>
          </span>
        </Col>
        <Col className="col-xl-8 ml-auto app-second-col">
          <div
            className={
              mode
                ? "d-xl-none navbar-toggle-button-dark"
                : " d-xl-none navbar-toggle-button-light "
            }
            style={{ padding: "7px", marginRight: "7px" }}
            onClick={() => dispatch({ type: "togglMode" })}
          >
            <ModeToggle />
          </div>

          <Navbar expand="xl">
            <Container>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Offcanvas >
              <Offcanvas.Body className={mode? 'd-flex justify-content-center align-items-center offcanvesDark' : 'd-flex justify-content-center align-items-center offcanvesLight'} >
                <Nav className="d-flex flex-column navbar-margin-padding ">
                  <div className="my-2 nav-responsive  navbar-links-hover">
                    {mode === false ? (
                      <img
                        src={HomeDarkIcon}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    ) : (
                      <img
                        src={HomeLightIcon}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    )}
                    <Link
                      className={
                        mode
                          ? "navbar-link-style-dark"
                          : "navbar-link-style-light"
                      }
                      to="/"
                    >
                      Home
                    </Link>
                  </div>
                  <div className="my-2 nav-responsive navbar-links-hover">
                    {mode === false ? (
                      <img
                        src={AboutDark}
                        style={{ height: "20px" }}
                        className="me-2"
                        alt="about"
                      />
                    ) : (
                      <img
                        src={Aboutlight}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    )}
                    <Link
                      className={
                        mode
                          ? "navbar-link-style-dark"
                          : "navbar-link-style-light"
                      }
                      to="/about"
                    >
                      About
                    </Link>
                  </div>
                  <div className="my-2 nav-responsive navbar-links-hover">
                    {mode === false ? (
                      <img
                        src={ServiceDark}
                        alt=""
                        style={{ height: "20px" }}
                        className="me-2"
                      />
                    ) : (
                      <img
                        src={ServiceLight}
                        alt=""
                        style={{ height: "20px" }}
                        className="me-2"
                      />
                    )}
                    <Link
                      className={
                        mode
                          ? "navbar-link-style-dark"
                          : "navbar-link-style-light"
                      }
                      to="/#"
                    >
                      Service
                    </Link>
                  </div>
                  <div className="my-2 nav-responsive navbar-links-hover">
                    {mode === false ? (
                      <img
                        src={PortfolioLight}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    ) : (
                      <img
                        src={PortfolioDark}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    )}
                    <Link
                      className={
                        mode
                          ? "navbar-link-style-dark"
                          : "navbar-link-style-light"
                      }
                      to="/#"
                    >
                      Portfolio
                    </Link>
                  </div>
                  <div className="my-2 nav-responsive navbar-links-hover">
                    {mode === false ? (
                      <img
                        src={loginDark}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    ) : (
                      <img
                        src={loginLight}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    )}
                    <Link
                      className={
                        mode
                          ? "navbar-link-style-dark"
                          : "navbar-link-style-light"
                      }
                      to="/login"
                    >
                      Log In/Register
                    </Link>
                  </div>
                  <div className="my-2 nav-responsive navbar-links-hover">
                    {mode === false ? (
                      <img
                        src={ContactLight}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    ) : (
                      <img
                        src={ContactDark}
                        style={{ height: "20px" }}
                        alt=""
                        className="me-2"
                      />
                    )}
                    <Link
                      className={
                        mode
                          ? "navbar-link-style-dark"
                          : "navbar-link-style-light"
                      }
                      to="/#"
                    >
                      Contact
                    </Link>
                  </div>
                  <p
                    style={
                      mode === true
                        ? {
                            marginTop: "2rem",
                            fontSize: "15px",
                            color: "#bbb",
                          }
                        : {
                            marginTop: "2rem",
                            fontSize: "15px",
                            color: "#000",
                          }
                    }
                    class="d-none d-md-block"
                  >
                    &copy; coded by MAJID in 2023
                  </p>
                </Nav>
               </Offcanvas.Body>
              </Navbar.Offcanvas>
            </Container>
          </Navbar>
        </Col>
      </Row>
    </Container>
  );
}

export default NavbarMenue;
