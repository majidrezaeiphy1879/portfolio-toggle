import "./App.css";
import { useReducer } from "react";
import {
  darkMode,
  navdarkmode,
  navlightmode,
  contentlightmode,
  toggleColumn,
  togglecolumnLight,
} from "./style/styles";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import NavbarMenue from "./components/Navbar";
import LoginRegister from "./pages/Login-Register";
import About from "./pages/About";
import Dashboard from "./components/Dashboard";
import Home from "./pages/Home";
import ModeToggle from "./components/ModeToggle";
import { modeContext } from "./context/ModeContext";

const initialState = {
  navbarStyle: navdarkmode,
  contentColStyle: darkMode,
  toggleCol: toggleColumn,
  isClicked: true,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "togglMode":
      return {
        ...state,
        navbarStyle: state.isClicked ? navlightmode : navdarkmode,
        contentColStyle: state.isClicked ? contentlightmode : darkMode,
        togglecol: state.isClicked ? togglecolumnLight : toggleColumn,
        isClicked: !state.isClicked,
      };
    default:
      return state;
  }
};

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <BrowserRouter>
      <modeContext.Provider
        value={{ mode: state.isClicked, reducer, dispatch }}
      >
        <Container fluid>
          <Row style={{ height: "100vh" }}>
            <Col xl={3} style={state.navbarStyle} className="app-nav-col">
              <NavbarMenue />
            </Col>
            <Col
              xl={8}
              className="app-col-style"
              style={state.isClicked ? contentlightmode : darkMode}
            >
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/about" element={<About />} />
                <Route path="/about" element={<About />} />
                <Route path="/about" element={<About />} />
                <Route path="/login" element={<LoginRegister />} />
                <Route path="/dashboard" element={<Dashboard />} />
              </Routes>
            </Col>
            <Col
              xl={1}
              className="app-toggle-mode p-0"
              style={state.isClicked ? togglecolumnLight : toggleColumn}
            >
              <div
                onClick={() => dispatch({ type: "togglMode" })}
                className="toggle-style app-nav-col"
              >
                <ModeToggle />
              </div>
            </Col>
          </Row>
        </Container>
      </modeContext.Provider>
    </BrowserRouter>
  );
}

export default App;
