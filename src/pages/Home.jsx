import { useContext } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import TypingEffect from "../components/TypingAnime";
import facebookDark from "../images/facebook-.svg";
import facbookLight from "../images/facebookLight.svg";
import instaDark from "../images/instagramDark.svg";
import instaLight from "../images/instagramLight.svg";
import twitterLight from "../images/twitterLight.svg";
import twitterDark from "../images/twitter-.svg";
import tiktokDark from "../images/tiktokDark.svg";
import tiktokLight from "../images/tiktokLight.svg";
import dribblerDark from "../images/dribbbleDark.svg";
import dribblerLight from "../images/dribbbleLight.svg";
import { motion } from "framer-motion";
import { modeContext } from "../context/ModeContext";
function Home() {
  const { mode } = useContext(modeContext);
  const item = ["Programmer", "Astrophysicist", "Developer"];
  return (
    <motion.div animate={{ x: 10, duratiion: 1 }}>
      <Container fluid>
        <Row className="flex-md-col flex-row content-style">
          <Col
            md={6}
            xs={12}
            className="d-flex align-items-center justify-content-center p-0"
          >
            <div className="image-border-style"></div>
          </Col>
          <Col
            md={6}
            xs={12}
            className="d-flex flex-column align-items-center p-0"
            style={{ marginTop: "1rem" }}
          >
            <span style={{ margin: "15px" }}>
              <h1 className={mode ? "home-MynameLight" : "home-MynameDark"}>
                MAJID REZAEI
              </h1>
              <TypingEffect words={item} className="typeanime" />
              <p
                className={mode ? "home-paragraphLight" : "home-paragraphDark"}
              >
                Creative person with a lot of interests. Coding, Math, Physics
                and learning
              </p>
              <div className="d-flex social-media-style mt-3">
                <a target="_blank" href="#" className="home-socialm-icon">
                  {mode ? (
                    <img
                      src={facbookLight}
                      className="me-1"
                      alt=""
                      style={{ height: "28px", widows: "28px" }}
                    />
                  ) : (
                    <img
                      src={facebookDark}
                      className="me-1"
                      alt=""
                      style={{ height: "30px", widows: "30px" }}
                    />
                  )}
                </a>
                <a target="_blank" href="#">
                  {mode ? (
                    <img
                      src={twitterLight}
                      alt=""
                      style={{ height: "22px", widows: "22px" }}
                      className="px-2"
                    />
                  ) : (
                    <img
                      src={twitterDark}
                      alt=""
                      style={{ height: "25px", widows: "25px" }}
                      className="px-2"
                    />
                  )}
                </a>
                <a target="_blank" href="#">
                  {mode ? (
                    <img
                      src={instaLight}
                      alt=""
                      style={{ height: "25px", widows: "25px" }}
                      className="px-2"
                    />
                  ) : (
                    <img
                      src={instaDark}
                      alt=""
                      style={{ height: "25px", widows: "25px" }}
                      className="px-2"
                    />
                  )}
                </a>
                <a target="_blank" href="#">
                  {mode ? (
                    <img
                      src={dribblerLight}
                      alt=""
                      style={{ height: "20px", widows: "20px" }}
                      className="px-2"
                    />
                  ) : (
                    <img
                      src={dribblerDark}
                      alt=""
                      style={{ height: "20px", widows: "20px" }}
                      className="px-2"
                    />
                  )}
                </a>
                <a target="_blank" href="#">
                  {mode ? (
                    <img
                      src={tiktokLight}
                      alt=""
                      style={{ height: "22px", widows: "22px" }}
                      className="px-2"
                    />
                  ) : (
                    <img
                      src={tiktokDark}
                      alt=""
                      style={{ height: "22px", widows: "22px" }}
                      className="px-2"
                    />
                  )}
                </a>
              </div>
            </span>
          </Col>
        </Row>
      </Container>
    </motion.div>
  );
}

export default Home;
