import React, { useEffect, useState, useContext } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import person from "../images/someone.jpg";
import Image from "react-bootstrap/Image";
import ListGroup from "react-bootstrap/ListGroup";
import { modeContext } from "../context/ModeContext";
import axios from "axios";
import { color } from "framer-motion";
const aboutData = {
  languages: ["Python", "Java Script", "SQL"],
  skills: ["React", "ExpressJS/NodeJS", "Bootstrap/Material UI"],
  Interests: ["Mathematics", "Physics", "AI", "Swimming"],
};

function About() {
  const { mode } = useContext(modeContext);
  const [number, setNumber] = useState("");
  const [fetchResult, setFetchResult] = useState([]);
  const handleNumber = (e) => {
    setNumber(e.target.value);
  };

  const items = ["math", "trivia", "year", "date"];

  useEffect(() => {
    setFetchResult([]);
  }, [number]);

  const fetchData = async (item) => {
    try {
      if (isNaN(number)) {
        return "not Valid";
      }
      const url = `http://numbersapi.com/${Math.floor(number)}/${item}`;
      const res = await axios.get(url);
      return res.data;
    } catch (error) {
      return "Error fetching data";
    }
  };

  const handleClick = async () => {
    if (isNaN(number)) {
      setFetchResult(["Enter a valid number"]);
    } else {
      try {
        const results = await Promise.all(items.map((item) => fetchData(item)));
        setFetchResult(results);
      } catch (error) {
        console.error("Error fetching data:", error);
        setFetchResult(["Error fetching data"]);
      }
    }
  };

  return (
    <Container fluid style={{ padding: "0" }}>
      <Row className="flex-md-col flex-row content-style">
        <Col>
          <Form>
            <Form.Text style={mode ? { color: "white" } : { color: "#000" }}>
              Enter a number and get some info about it!
            </Form.Text>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInput1"
              onChange={handleNumber}
            >
              <Form.Control type="number" id="typeNumber"  />
            </Form.Group>
            <Button onClick={handleClick}>Interesting Info</Button>
            {number && fetchResult.length && (
              <p className="api-p m-2 mt-4 p-2">
                {fetchResult.map((result) => (
                  <p style={mode ? { color: "white",fontSize:'18px' } : { color: "#000",fontSize:'18px' }}>{result}</p>
                ))}
              </p>
            )}
          </Form>

          <Row>
            <h5 className="mb-4 mt-2" style={mode ? {color:'#fff'}:{color:'#000'}} >About Me</h5>
            <Image src={person} rounded />
            <span className="aboutpage mt-2 mb-1">
              <h4 style={mode ? {color:'#fff'}:{color:'#000'}} className="m-2">Majid Rezaei</h4>
            </span>
            <hr
              className="aboutpage"
              style={{ border: "0px", borderTop: "1px solid #CCC" }}
            />
            <span className="text-primary pb-3 fw-bold" style={{fontSize:'17px'}}>
              Hi my name is majid. i love conding. Lorem ipsum dolor sit, amet
              consectetur adipisicing elit. Porro voluptas nihil rerum eveniet
              eligendi suscipit praesentium, unde, dolores modi autem eaque?
              Corporis possimus aspernatur veniam iusto obcaecati, sequi, ut
              molestias blanditiis animi optio eos autem odit pariatur
              voluptatibus quidem tenetur, suscipit ullam? Impedit qui facilis
              aperiam reiciendis eveniet repudiandae blanditiis!
            </span>
            <hr
              className="aboutpage"
              style={{ border: "0px", borderTop: "1px solid #CCC" }}
            />
          </Row>

          <Button>Download CV</Button>

          <Container
            className="mt-4 p-2"
            style={{ backgroundColor: "#000", borderRadius: "10px" }}
          >
            <Row>
              <Col style={{ color: "white" }}>
                <h5 className="m-1 mb-3">Programming Skills</h5>
                <ListGroup>
                  {aboutData.languages.map((item) => (
                    <ListGroup.Item className="text-success fw-bold">{item}</ListGroup.Item>
                  ))}
                </ListGroup>
              </Col>
              <Col style={{ color: "white" }}>
                <h5 className="m-1 mb-3">Knowledge</h5>
                <ListGroup>
                  {aboutData.skills.map((item) => (
                    <ListGroup.Item className="text-success fw-bold">{item}</ListGroup.Item>
                  ))}
                </ListGroup>
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
    </Container>
  );
}

export default About;
