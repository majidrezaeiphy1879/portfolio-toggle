import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Login from "../components/Login";
import Register from "../components/Register";




function LoginRegister() {
  const [isRegister, setRegister] = useState(true);

  const toggleRegister = () => {
    setRegister((prevstate) => !isRegister);
  };
  return (
    <Container>
      <Row>
        <Col>
          {isRegister ? (
            <Register toggleRegister={toggleRegister} />
          ) : (
            <Login toggleRegister={toggleRegister} />
          )}
        </Col>
      </Row>
    </Container>
  );
}

export default LoginRegister;
